﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication4.Model
{
    public class Person
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Patronymic { get; set; }

        public override string ToString()
        {
            return $"{SecondName} {FirstName} {Patronymic}";
        }
    }
}
