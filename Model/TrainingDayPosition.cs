﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.Model
{
    public class TrainingDayPosition
    {
        public PhysicalExercise Exercise { get; set; }
        public int Approaches { get; set; }
        public int Reiterations { get; set; }
    }
}
