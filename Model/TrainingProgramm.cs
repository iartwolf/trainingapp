﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.Model
{
    public class TrainingProgramm
    {
        public string Name { get; set; }
        public DateTime DateCreated { get; set; }
        public List<TrainingDay> TrainingDays { get; set; }
        public Person Author { get; set; }
    }
}
