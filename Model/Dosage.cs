﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.Model
{
    public class Dosage
    {
        public int ApproachCount { get; set; }
        public int ReiterationCount { get; set; }
    }

    public class ComplexDosage
    {
        public List<int> Approches { get; set; }
    }
    
}
