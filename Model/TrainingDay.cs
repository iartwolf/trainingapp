﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.Model
{
    /// <summary>
    /// тренировочный день
    /// </summary>
    public class TrainingDay
    {
        public DayOfWeek Day { get; set; }
        public List<TrainingDayPosition> Exercises { get; set; }
    }
}
