﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.Model
{
    /// <summary>
    /// Упражнение
    /// </summary>
    public class PhysicalExercise
    {
        public string Name { get; set; }
        public PhysicalExerciseType Type { get; set; }
    }
}
