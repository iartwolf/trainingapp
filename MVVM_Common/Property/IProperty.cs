﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WpfApplication4.Model;
using WpfApplication4.ViewModel;

namespace WpfApplication4.MVVM_Common.Property
{
    public abstract class Property
    {
        
    }

    public class StringProperty : ViewModelBase
    {
        private string val;

        public string Name { get; private set; }

        public string Value
        {
            get { return GetValue(); }
            set { SetValue(value); }
        }

        public List<string> Errors { get; set; }

        private Func<string> _getter;
        private Action<string> _setter;

        public StringProperty(string name, Func<string> getter, Action<string> setter)
        {
            Name = name;
            _getter = getter;
            _setter = setter;
            val = _getter.Invoke();
        }

        public bool NeedValidate { get; set; }

        private bool Validate()
        {
            return true;
        }

        private string GetValue()
        {
            return val;

            //return _getter.Invoke();
        }

        private void SetValue(string value)
        {
            if(val == value) return;
            if(NeedValidate && !Validate()) return;
            val = value;
            SetValue(val);
            FirePropertyChanged("Value");
            _setter.Invoke(value);
        }
    }

    public class PropertyCollection
    {

    }

    public class SomeClass
    {
        public void SomeMethod()
        {
            TrainingProgramm programm = new TrainingProgramm();

            StringProperty nameProp = new StringProperty(
                "Имя", 
                () => programm.Name, 
                str => programm.Name = str
                );
        }
    }
}
