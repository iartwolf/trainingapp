﻿using System.Collections;
using System.Windows.Controls;

namespace WpfApplication4
{
    /// <summary>
    /// Логика взаимодействия для ContentListBox.xaml
    /// </summary>
    public partial class ContentListBox : UserControl
    {

        public IEnumerable ItemsSource
        {
            get { return listBox.ItemsSource; }
            set { listBox.ItemsSource = value; }
        }

        private ContentPage page;

        public ContentListBox()
        {
            InitializeComponent();

            page = new ContentPage();

            page.ClickBack += Page_ClickBack;
            page.ClickAccept += Page_ClickAccept;

            listBox.SelectionChanged += ListBox_SelectionChanged;

             
        }

        private void Page_ClickAccept(object sender, System.Windows.RoutedEventArgs e)
        {
            listBox.SelectedItem = null;
        }

        private void Page_ClickBack(object sender, System.Windows.RoutedEventArgs e)
        {
            listBox.SelectedItem = null;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(listBox.SelectedItem != null)
            {
                contentControl.Content = page;
                page.Content = listBox.SelectedItem;
            }
            else
            {
                contentControl.Content = listBox;
            }
        }
    }
}
