﻿using System.Collections.Generic;
using System.Windows;
using WpfApplication4.Command;
using WpfApplication4.Model;
using WpfApplication4.Services;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.ViewModel
{
    public class ProgrammsListPage : PageViewModel
    {
        private List<TrainingProgramm> programs;
        public List<TrainingProgramm> Programms => programs;

        public TrainingProgramm SelectedProgramm
        {
            get { return null; }
            set
            {
                if(value == null) return;
                NavigationService.GoTo<ProgrammPage>(value);
            }
        }

        public override string Title => "Список программы";

        private List<MenuItemViewModel> menuItems = new List<MenuItemViewModel>();
        public override List<MenuItemViewModel> MenuItems => menuItems;

        public override bool CanGoBack => false;

        public ProgrammsListPage()
        {
            menuItems.Add(new MenuItemViewModel()
            {
                Header = "Добавить новую",
                Command = new RelayCommand(AddNew)
            });
            menuItems.Add(new MenuItemViewModel()
            {
                Header = "Выход",
                Command = new RelayCommand(Close)
            });
            programs = DataService.GetProgramms();
        }

        private void AddNew()
        {
            NavigationService.GoTo<CreateProgrammPage>();
        }

        private void Close()
        {
            MessageBoxResult res = MessageBox.Show("Вы уверены?", "Выход из приложения", MessageBoxButton.YesNo);
            if (res == MessageBoxResult.Yes)
            {
                System.Windows.Application.Current.Shutdown();
            }
        }

        public override void GoBack()
        {
            //throw new NotImplementedException();
        }
    }
}
