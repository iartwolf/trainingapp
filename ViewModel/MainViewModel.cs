﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.ViewModel
{
    public class MainViewModel : ViewModelBase
    {

        private PageViewModel currentPage;
        public PageViewModel CurrentPage
        {
            get { return currentPage; }
            set
            {
                if (currentPage != value)
                {
                    currentPage = value;
                    FirePropertyChanged("CurrentPage");
                }
            }
        }


    }
}
