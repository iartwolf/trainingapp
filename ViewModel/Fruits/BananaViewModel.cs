﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.ViewModel
{
    public class BananaViewModel : FruitViewModel
    {
        public override string Name => "Банан";
    }
}
