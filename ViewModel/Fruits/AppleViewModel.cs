﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfApplication4.ViewModel
{
    public class AppleViewModel : FruitViewModel
    {
        public override string Name => "Яблоко";
    }
}
