﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace WpfApplication4.ViewModel
{
    public class MenuItemViewModel : ViewModelBase
    {
        public string Header { get; set; }
        public ICommand Command { get; set; }
        public object CommandParameter { get; }
    }
}
