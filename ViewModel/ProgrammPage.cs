﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WpfApplication4.Command;
using WpfApplication4.Contex;
using WpfApplication4.Model;
using WpfApplication4.Services;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.ViewModel
{
    public class ProgrammPage : PageViewModel
    {
        private List<MenuItemViewModel> menuItems;
        public override List<MenuItemViewModel> MenuItems => menuItems;

        public override string Title => model.Name;

        public override bool CanGoBack => true;

        //private TrainingProgramm trainingProgramm;
        //public TrainingProgramm TrainingProgramm
        //{
        //    get { return trainingProgramm; }
        //    set { trainingProgramm = value; }
        //}

        public DateTime DateCreated => model.DateCreated;

        public List<TrainingDay> TrainingDays => model.TrainingDays;

        private TrainingProgramm model;
        public override object Model
        {
            get { return model; }
            set
            {
                if (value is TrainingProgramm)
                {
                    model = value as TrainingProgramm;
                }
            }
        }

        public ProgrammPage()
        {
            menuItems = new List<MenuItemViewModel>();
            menuItems.Add(new MenuItemViewModel()
            {
                Header = "Изменить",
                Command = new RelayCommand(Edit)
            });
        }

        private void Edit()
        {
            //NavigationService.GoTo(PageFactory.GetEditProgrammPage());
        }

    }
}
