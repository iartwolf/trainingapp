﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using WpfApplication4.Command;
using WpfApplication4.Services;

namespace WpfApplication4.ViewModel.Pages
{
    public abstract class PageViewModel : ViewModelBase
    {
        public abstract bool CanGoBack { get; }

        public abstract string Title { get; }
        public abstract List<MenuItemViewModel> MenuItems { get; }
        public bool MenuVisible => !(MenuItems == null || MenuItems.Count == 0);


        public virtual void GoBack()
        {
            NavigationService.GoBack();
        }

        public ICommand GoBackCommand { get; private set; }


        public virtual object Model { get; set; }

        public PageViewModel()
        {
            GoBackCommand = new RelayCommand(GoBack);
        }

    }
}
