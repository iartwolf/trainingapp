﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using WpfApplication4.Command;
using WpfApplication4.Contex;
using WpfApplication4.Model;
using WpfApplication4.Services;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.ViewModel
{
    public class CreateProgrammPage : PageViewModel
    {
        #region page_properties
        public override string Title => "Создание новое программы";
        public override List<MenuItemViewModel> MenuItems => null;
        public override bool CanGoBack => true;
        #endregion page_properties


        public ICommand CreateCommand { get; private set; }
        public ICommand AddTrainingDayCommand { get; private set; }
        
        public string Name { get; set; }

        private DateTime dateCreated;
        public DateTime DateCreated {
            get
            {
                return dateCreated;
            }
            set
            {
                dateCreated = value;
            }
        }

        public List<Person> Authors { get; private set; }
        public Person Author { get; set; }

        private List<TrainingDay> trainingDays = new List<TrainingDay>();
        public List<TrainingDay> TrainingDays => trainingDays;

        public CreateProgrammPage()
        {
            DateCreated = DateTime.Now;
            CreateCommand = new RelayCommand(Create);
            Authors = DataService.GetAuthors();

            AddTrainingDayCommand = new RelayCommand(AddTrainingDay);
        }

        private void Create()
        {
            List<TrainingProgramm> programms = DataService.GetProgramms();

            programms.Add(new TrainingProgramm()
            {
                Name = this.Name,
                DateCreated = this.DateCreated,
                Author = this.Author,
                TrainingDays = new List<TrainingDay>()
            });

            NavigationService.GoTo<ProgrammsListPage>();
        }

        public void AddTrainingDay()
        {
            NavigationService.GoTo<CreateTrainingDayPage>(trainingDays);
        }

        public override void GoBack()
        {
            NavigationService.GoTo<ProgrammsListPage>();
        }
    }
}
