﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using WpfApplication4.Command;
using WpfApplication4.Model;
using WpfApplication4.Services;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.ViewModel
{
    public class CreateTrainingDayPage : PageViewModel
    {
        public override bool CanGoBack => true;
        public override List<MenuItemViewModel> MenuItems => null;
        public override string Title => "Новый тренировочный день";

        private List<TrainingDay> model;
        public override object Model
        {
            get { return model; }
            set
            {
                if (value is List<TrainingDay>)
                {
                    model = value as List<TrainingDay>;
                }
            }
        }

        public List<DayOfWeek> Days { get; private set; }
        public DayOfWeek? Day { get; set; }

        public ICommand CreateCommand { get; private set; }

        private List<TrainingDayPosition> exercises = new List<TrainingDayPosition>();
        public List<TrainingDayPosition> Exercises => exercises;

        public CreateTrainingDayPage()
        {
            Days = new List<DayOfWeek>();
            Days.Add(DayOfWeek.Monday);
            Days.Add(DayOfWeek.Tuesday);
            Days.Add(DayOfWeek.Wednesday);
            Days.Add(DayOfWeek.Thursday);
            Days.Add(DayOfWeek.Friday);
            Days.Add(DayOfWeek.Saturday);
            Days.Add(DayOfWeek.Sunday);

            CreateCommand = new RelayCommand(Create);
        }

        private void Create()
        {
            if (Day.HasValue && model != null)
            {
                TrainingDay trainingDay = new TrainingDay()
                {
                    Day = this.Day.Value,
                    Exercises = this.Exercises
                };
                model.Add(trainingDay);
                NavigationService.GoBack();
            }
             
        }

    }
}
