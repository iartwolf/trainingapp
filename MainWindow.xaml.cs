﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using WpfApplication4.Contex;
using WpfApplication4.Services;
using WpfApplication4.ViewModel;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MainViewModel vm;

        public MainWindow()
        {
            InitializeComponent();
            NavigationService.Init(mainContent);
            NavigationService.GoTo<ProgrammsListPage>();
        }

    }
}
