﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WpfApplication4.ViewModel;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.Contex
{
    public class PageFactory
    {

        private static EditProgrammPage editProgrammPage = new EditProgrammPage();
        public static PageViewModel GetEditProgrammPage()
        {
            return editProgrammPage;
        }

        private static CreateProgrammPage createProgrammPage = new CreateProgrammPage();
        public static  PageViewModel GetCreateProgrammPage()
        {
            return createProgrammPage;
        }

        private static ProgrammsListPage programmListPage = new ProgrammsListPage();
        public static  PageViewModel GetProgrammListPage()
        {
            return programmListPage;
        }

        private static ProgrammPage programmViewerPage = new ProgrammPage();
        public static  PageViewModel GetProgrammViewerPage()
        {
            return programmViewerPage;
        }
    }
}
