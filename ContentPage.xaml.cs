﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication4
{
    /// <summary>
    /// Логика взаимодействия для ContentPage.xaml
    /// </summary>
    public partial class ContentPage : UserControl
    {

        public new object Content
        {
            get
            {
                return pageContent.Content;
            }
            set
            {
                pageContent.Content = value;
            }
        }

        public ContentPage()
        {
            InitializeComponent();
        }

        public event RoutedEventHandler ClickBack
        {
            add { backButton.Click += value; }
            remove { backButton.Click -= value; }
        }

        public event RoutedEventHandler ClickAccept
        {
            add { acceptButton.Click += value; }
            remove { acceptButton.Click -= value; }
        }

    }
}
