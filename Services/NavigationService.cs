﻿using Ninject;
using System.Windows.Controls;
using WpfApplication4.ViewModel.Pages;

namespace WpfApplication4.Services
{
    public class NavigationService
    {
        private static ContentControl cont;

        private static object prevPage = null;


        public static void GoTo(PageViewModel page)
        {
            prevPage = cont.Content;
            cont.Content = page;
        }

        public static void GoTo(PageViewModel page, object model)
        {
            page.Model = model;
            prevPage = cont.Content;
            cont.Content = page;
        }

        public static void GoTo<T>() where T: PageViewModel
        {
            PageViewModel viewModel = IoCService.Kernel.Get<T>();
            prevPage = cont.Content;
            cont.Content = viewModel;
        }

        public static void GoTo<T>(object model) where T : PageViewModel
        {
            PageViewModel viewModel = IoCService.Kernel.Get<T>();
            viewModel.Model = model;
            prevPage = cont.Content;
            cont.Content = viewModel;
        }

        public static T Resolve<T>() where T : PageViewModel
        {
            return IoCService.Kernel.Get<T>();
        }

        public static void GoBack()
        {
            cont.Content = prevPage;
        }

        public static void Init(ContentControl container)
        {
            cont = container;
        }
    }
}
