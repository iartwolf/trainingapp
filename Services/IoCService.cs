﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication4.ViewModel;

namespace WpfApplication4.Services
{
    public class IoCService
    {

        static StandardKernel kernel;

        public static StandardKernel Kernel => kernel;

        static IoCService()
        {
            kernel = new StandardKernel();
            kernel.Bind<CreateProgrammPage>().ToSelf().InSingletonScope();

            kernel.Bind<CreateTrainingDayPage>().ToSelf().InSingletonScope();

            kernel.Bind<EditProgrammPage>().ToSelf().InSingletonScope();
            kernel.Bind<ProgrammsListPage>().ToSelf().InSingletonScope();
            kernel.Bind<ProgrammPage>().ToSelf().InSingletonScope();
        }
              
    }
}
