﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApplication4.Model;

namespace WpfApplication4.Services
{
    public class DataService
    {
        static DataService()
        {
            authors.Add(new Person()
            {
                FirstName = "Иван",
                SecondName = "Иванов",
                Patronymic = "Иванович"
            });
            authors.Add(new Person()
            {
                FirstName = "Петр",
                SecondName = "Николаев",
                Patronymic = "Сидоров"
            });
            authors.Add(new Person()
            {
                FirstName = "Мария",
                SecondName = "Лопухова",
                Patronymic = "Анатольевна"
            });

            List<PhysicalExercise> exercises = new List<PhysicalExercise>();

            exercises.Add(new PhysicalExercise() { Name = "Жим лежа" });
            exercises.Add(new PhysicalExercise() { Name = "Жим 45 градусов" });
            exercises.Add(new PhysicalExercise() { Name = "Жим в хаммере" });

            exercises.Add(new PhysicalExercise() { Name = "Пресс лежа" });
            exercises.Add(new PhysicalExercise() { Name = "Пресс в висе" });
            exercises.Add(new PhysicalExercise() { Name = "Пресс на тренажере" });


            TrainingProgramm p1 = new TrainingProgramm();

            p1.Name = "Первая программа";
            p1.DateCreated = DateTime.Now;

            TrainingDay p1td1 = new TrainingDay();
            p1td1.Day = DayOfWeek.Tuesday;
            p1td1.Exercises = new List<TrainingDayPosition>();
            p1td1.Exercises.Add(new TrainingDayPosition()
            {
                Exercise = exercises[0],
                Approaches = 4,
                Reiterations = 10
            });
            p1td1.Exercises.Add(new TrainingDayPosition()
            {
                Exercise = exercises[3],
                Approaches = 4,
                Reiterations = 10
            });

            TrainingDay p1td2 = new TrainingDay();
            p1td2.Day = DayOfWeek.Thursday;
            p1td2.Exercises = new List<TrainingDayPosition>();
            p1td2.Exercises.Add(new TrainingDayPosition()
            {
                Exercise = exercises[1],
                Approaches = 4,
                Reiterations = 10
            });
            p1td2.Exercises.Add(new TrainingDayPosition()
            {
                Exercise = exercises[4],
                Approaches = 4,
                Reiterations = 10
            });

            TrainingDay p1td3 = new TrainingDay();
            p1td3.Day = DayOfWeek.Saturday;
            p1td3.Exercises = new List<TrainingDayPosition>();
            p1td3.Exercises.Add(new TrainingDayPosition()
            {
                Exercise = exercises[2],
                Approaches = 4,
                Reiterations = 10
            });
            p1td3.Exercises.Add(new TrainingDayPosition()
            {
                Exercise = exercises[5],
                Approaches = 4,
                Reiterations = 10
            });

            p1.TrainingDays = new List<TrainingDay>();

            p1.TrainingDays.Add(p1td1);
            p1.TrainingDays.Add(p1td2);
            p1.TrainingDays.Add(p1td3);

            p1.Author = authors[0];

            programms.Add(p1);


            FillPhysicalExerciseTypes();
            FillPhysicalExercises();
        }

        private static List<Person> authors = new List<Person>();

        public static List<Person> GetAuthors()
        {
            return authors;
        }

        private static List<TrainingProgramm> programms = new List<TrainingProgramm>();

        public static List<TrainingProgramm> GetProgramms()
        {
            return programms;
        }

        private static List<PhysicalExerciseType> physicalExerciseTypes = new List<PhysicalExerciseType>();

        public static List<PhysicalExerciseType> GetPhysicalExerciseTypes()
        {
            return physicalExerciseTypes;
        }

        private static void FillPhysicalExerciseTypes()
        {
            physicalExerciseTypes.Add(new PhysicalExerciseType() { Name = "Силовое" });
            physicalExerciseTypes.Add(new PhysicalExerciseType() { Name = "Кардио" });
            physicalExerciseTypes.Add(new PhysicalExerciseType() { Name = "Гибкость" });
        }

        private static List<PhysicalExercise> physicalExercises = new List<PhysicalExercise>();

        public static List<PhysicalExercise> GetPhysicalExercises()
        {
            return physicalExercises;
        }

        private static void FillPhysicalExercises()
        {

            physicalExercises.Add(new PhysicalExercise()
            {
                Name = "Жим штанги лежа",
                Type = physicalExerciseTypes[0]
            });
            physicalExercises.Add(new PhysicalExercise()
            {
                Name = "Бег",
                Type = physicalExerciseTypes[1]
            });
            physicalExercises.Add(new PhysicalExercise()
            {
                Name = "Растяжка на шаре",
                Type = physicalExerciseTypes[2]
            });

            physicalExercises.Add(new PhysicalExercise()
            {
                Name = "Бицепс стоя",
                Type = physicalExerciseTypes[0]
            });
            physicalExercises.Add(new PhysicalExercise()
            {
                Name = "Велотренажер",
                Type = physicalExerciseTypes[1]
            });
            physicalExercises.Add(new PhysicalExercise()
            {
                Name = "Шпагат на тренажере",
                Type = physicalExerciseTypes[2]
            });

        }
    }
}
