﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfApplication4.Services;
using WpfApplication4.View;
using WpfApplication4.ViewModel;

namespace WpfApplication4.View
{
    /// <summary>
    /// Логика взаимодействия для PageView.xaml
    /// </summary>
    public partial class PageView : UserControl
    {
        public PageView()
        {
            InitializeComponent();

            //CreateProgrammPage NewTrainingProgrammView

            //ProgrammsListPage ProgrammsListView

            //ProgrammPage ProgrammView

            Console.WriteLine("Hello, World!");

            TemplateService.Register<CreateProgrammPage, NewTrainingProgrammView>(Resources);
            TemplateService.Register<ProgrammsListPage, ProgrammsListView>(Resources);
            TemplateService.Register<ProgrammPage, ProgrammView>(Resources);
            TemplateService.Register<CreateTrainingDayPage, CreateTrainingDayView>(Resources);
        }
    }
}
